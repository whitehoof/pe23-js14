"use strict"

// читаю та застосовую збережену (або дефолтну світлу) тему:
document.onload = retrieveTheme();

// вішаю слухач на іконку зміни теми:
const switcher = document.querySelector(".dark-mode-toggle-icon");
switcher.addEventListener("click", () => {
	toggleTheme();
});



// зберігання теми в localStorage:
function storeTheme(theme) {
	localStorage.setItem("theme", theme)
};



// читання теми з localStorage та застосування
function retrieveTheme() {
	let theme = localStorage.getItem("theme");
	if(theme === null) {
		setTheme("light");
		storeTheme("light");
	} else {
		setTheme(theme);
		if (theme === "dark") {
			imgInvert(70);
		};
	};
	
};

// застосування теми до елементу html:
function setTheme(theme) {
	document.documentElement.className = theme;
};



// переключення теми (якщо тему не вказано в localStorage, туди зберігається "light"):
function toggleTheme() {
	if (document.documentElement.classList.contains("light")) {
		document.documentElement.className = "dark";
		imgInvert(70);
		storeTheme("dark");
	} else {
		document.documentElement.className = "light";
		imgInvert(0);
		storeTheme("light");
	};
};



// інвертування всіх картинок:
function imgInvert(n) {
	document.querySelectorAll("img").forEach( el => {
		el.style.filter = `invert(${n}%)`;
	});
}
